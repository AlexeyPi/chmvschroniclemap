import net.openhft.chronicle.core.values.DoubleValue;
import net.openhft.chronicle.map.ChronicleMap;
import net.openhft.chronicle.values.Values;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class MainApp {

    public static final int ENTRIES = 10_000_000;
    public static final int READ_COUNT = 100_000;
    //simple compound key (we can't get all entries for PK)
    private String keyPrefix = "PK:SecondaryKey";
    private String key = "PK:SecondaryKey:EventDate";


    private ConcurrentHashMap<String, Double> chm;
    private ChronicleMap<String, Double> chronicleMap;
    private ChronicleMap<String, DoubleValue> chronicleMapWithCustomDouble;

    public static void main(String[] args) {
        MainApp app = new MainApp();
        app.initTest();
        app.startTest();

    }

    private void initTest() {
        long start = System.currentTimeMillis();
        chm = createCHM();
        System.out.println("CHM populate in " + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        chronicleMap = createChronicleMap();
        System.out.println("Chronicle populate in " + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        chronicleMapWithCustomDouble = createChronicleMapCustomDouble();
        System.out.println("ChronicleCustomDouble populate in " + (System.currentTimeMillis() - start));

        System.gc();
    }

    private void startTest() {
        Random rnd = new Random();
        int[] rndArray = new int[READ_COUNT];
        for (int i = 0; i < READ_COUNT; i++)
            rndArray[i] = rnd.nextInt(ENTRIES);


        Double res = 0d;
        for (int i = 0; i < 10; i++) {
            res += measure(chm, "CHM",rndArray);

            res += measure(chronicleMap, "Chronicle", rndArray);

            res += measureCustomDouble(chronicleMapWithCustomDouble, "ChronicleCustomDouble");
        }
        System.out.println("Res=" + res);
    }

    private Double measure(Map<String, Double> map, String testName, int[] rndArray) {
        Double sum = 0d;
        Random rnd = new Random();
        long start = System.currentTimeMillis();
        for (int i = 0; i < READ_COUNT; i++) {
            Double val = map.get(keyPrefix + rnd.nextInt(ENTRIES));
            sum += val;

//            Double val = rnd.nextDouble();
//            Double val = map.get(keyPrefix + rndArray[i]);
        }
        long result = System.currentTimeMillis() - start;
        System.out.println(testName + " took " + result + " ms");
        return sum;
    }

    private Double measureCustomDouble(Map<String, DoubleValue> map, String testName) {
        double sum = 0d;
        Random rnd = new Random();
        long start = System.currentTimeMillis();
        for (int i = 0; i < READ_COUNT; i++) {
            double val = map.get(keyPrefix + rnd.nextInt(ENTRIES)).getValue();
            sum += val;
        }
        long result = System.currentTimeMillis() - start;
        System.out.println(testName + " took " + result + " ms");
        return sum;
    }

    public ConcurrentHashMap<String, Double> createCHM() {
        Random rnd = new Random();
        ConcurrentHashMap<String, Double> result = new ConcurrentHashMap(ENTRIES, 1.0f);

        for (int i = 0; i < ENTRIES; i++) {
            result.put(keyPrefix + i, rnd.nextDouble());
        }

        return result;
    }

    public ChronicleMap<String, Double> createChronicleMap() {
        Random rnd = new Random();
        ChronicleMap<String, Double> result = ChronicleMap.of(String.class, Double.class).
                averageKeySize(key.length()).
                entries(ENTRIES).
                create();

        for (int i = 0; i < ENTRIES; i++) {
            result.put(keyPrefix + i, rnd.nextDouble());
        }

        return result;
    }

    public ChronicleMap<String, DoubleValue> createChronicleMapCustomDouble() {
        Random rnd = new Random();
        ChronicleMap<String, DoubleValue> result = ChronicleMap.of(String.class, DoubleValue.class).
                averageKeySize(key.length()).
                entries(ENTRIES).
                create();

        for (int i = 0; i < ENTRIES; i++) {
            DoubleValue value = Values.newHeapInstance(DoubleValue.class);
            value.setValue(rnd.nextDouble());
            result.put(keyPrefix + i, value);
        }

        return result;
    }


}
